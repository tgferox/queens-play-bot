const { channel } = require('diagnostics_channel');
const { MessageEmbed, Client, Intents, Channel, MessageReaction, Guild } = require('discord.js');
const fs = require('fs')
var data = require('./data.json');

const client = new Client({ intents: ["GUILDS", "GUILD_MESSAGES", "DIRECT_MESSAGES", "GUILD_BANS", "GUILD_MEMBERS", ""], partials: ["CHANNEL"] });

client.on('ready', client => {
  client.user.setActivity("?help"); 
  console.log(`Logged in as ${client.user.tag}!`);
  var intervalId = setInterval(function(){
    client.channels.fetch('951569755167408162')
      .then(channel => channel.sendTyping())
      .catch(console.error)
  }, 10000);
  
});

const contains = (arr = [], m) => arr.some((txt) => m.includes(txt));

function log_message(message, member){
  if (member != null){
      client.channels.fetch('962486338660208650')
    .then(channel => {
      channel.send(`<@` + member.id + `> ` + message)
      channel.send("``` ```")
    })
  }
}

const hugGifs = ["https://c.tenor.com/hacbVpDut3sAAAAC/hug.gif",
                "https://c.tenor.com/8Jk1ueYnyYUAAAAC/hug.gif",
                "https://c.tenor.com/qF7mO4nnL0sAAAAC/abra%C3%A7o-hug.gif",
                "https://c.tenor.com/XyMvYx1xcJAAAAAC/super-excited.gif"];

const kissGifs = ["https://c.tenor.com/6Da_kxj3TLgAAAAC/kiss.gif",
                  "https://c.tenor.com/e6cYiAPPCq4AAAAC/anime-kissing.gif",
                  "https://c.tenor.com/N1fgjwPF-B8AAAAC/kiss-sweet.gif",
                  "https://c.tenor.com/06lz817csVgAAAAd/anime-anime-kiss.gif",
                  "https://c.tenor.com/s1VvsszCbCAAAAAC/love-you.gif",
                  "https://c.tenor.com/v4Ur0OCvaXcAAAAd/koi-to-uso-kiss.gif"];

const marryGifs = ["https://c.tenor.com/u7B_BCacat8AAAAC/wedding-ring-engaged.gif",
                  "https://c.tenor.com/UnSlrdcbV9kAAAAC/anime-ring.gif",
                  "https://c.tenor.com/X7w4MCZRMpMAAAAC/brown-cony.gif"];

const slapGifs = ["https://c.tenor.com/Ws6Dm1ZW_vMAAAAC/girl-slap.gif",
                  "https://c.tenor.com/WRYiTfeCU8YAAAAd/anime-slap-slap-anime.gif",
                  "https://c.tenor.com/qvvKGZhH0ysAAAAC/anime-girl.gif",
                  "https://c.tenor.com/EfhPfbG0hnMAAAAC/slap-handa-seishuu.gif"];

client.on('messageDelete', message => {
  console.log('messageDelete')
  messageString = message.content

  if (!(messageString.startsWith("?id") || (messageString.startsWith("?av"))) ) {
    console.log('uwu')
    console.log(messageString)
    console.log(messageString.startsWith("?id"))
  log_message('deleted ' + messageString, message.member);
}})
//Welcome message
client.on('guildMemberAdd', member => {
  client.channels.fetch('964598963862929480')
    .then(channel => {
      channel.send(`Welcome <@` + member.id + `> to **Queens Play!**\n*Enjoy your stay here, You're free to leave at any time.*`)
      channel.send("``` ```")
    })
})

client.on('messageCreate', message => {

  var messageString = message.content.toLowerCase();

//Checks if user is AFK and removes AFK status
  if (message.member != null){
    if (message.member.nickname != null){
      if (message.member.nickname.startsWith("[AFK]") ){
        message.member.setNickname(message.member.nickname.slice(5));
      }
    }
  }

//Checks message for urls
  var urlRegex = /(((https?:\/\/)|(www\.))[^\s]+)/g;
  if (message.member != null){
    if (!message.member.permissions.has("ADMINISTRATOR")){
      if (!message.member.roles.cache.has('957367934353817620')){
        if (messageString.match(urlRegex)){
          log_message('said ' + messageString, message.member);
          message.member.send('Please do not post links in chat.').catch(console.error)
          message.delete().catch(console.error);
        }
      }
    }
  }
  
  
//Checks for users using banned words
  var bannedWords = ["kill yourself","kys","when did i ask","who asked","negro","f@ggot","nigger","niggers","niggerz","nigga","niggor","nigs","nigz","niggas","niggaz","fag","fags","fagz","faggot","faggots","faggotz","queer","queers","queerz","nigg"]
  if (message != null){
    if (message.member != null){
      if (!message.member.permissions.has("ADMINISTRATOR")){
        if (message.mentions.members != null){
          if (contains(bannedWords, messageString.toLowerCase())){
            log_message('said ' + messageString, message.member);
            message.delete().catch(console.error);
            message.member.send('This is a Warning! Please do not use this word in chat.').catch(console.error)
          }
        }
      }
    }
  }

//Checks for users pinging Becky
  if (message.type != 'REPLY'){
    if (message != null){
      if (message.member != null){
        if (!message.member.permissions.has("ADMINISTRATOR")){
          if (message.mentions.members != null){
            mention = message.mentions.members.first()
            if (mention != null){
              if (mention.id == '863081824401621033'){
                if (!(messageString.startsWith("?id") || messageString.startsWith("?av") || messageString.startsWith("?hug") || messageString.startsWith("?marry") || messageString.startsWith("?slap") || messageString.startsWith("?kiss")) ) {
                  message.member.timeout(1800000, 'Dont ping ' + mention.displayName)
                  message.member.send("You have been timed out from Queen's Play for pinging " + mention.displayName).catch(console.error)
                  const beckyMention = new MessageEmbed()
                    .setColor(0xFFA500)
                    .setTitle(message.member.displayName + ' Timed out for 30 mins')
                  message.channel.send({ embeds: [beckyMention] });
                  message.channel.send('<@' + message.member.id + "> Don't ping " + mention.displayName)
                }
              }
            }
          }
        }   
      }
    } 
  }

//Checks for people asking for Dms
  var dmPhrases = ["may we dm", "mind if i dm", "can we dm", "allow me to dm", "may i dm", "can i dm", "wanna dm", "look at dm"]
  if (message != null){
    if (message.member != null){
      if (!message.member.permissions.has("ADMINISTRATOR")){
        if (contains(dmPhrases, messageString.toLowerCase()) && message.channelId == 951569755167408162){
          log_message('said ' + messageString, message.member);
          message.delete().catch(console.error);
          message.member.send(`**If you want to dm a queen go into their channel and ask to dm them**
**If its a servant/loyal/lodestar you want to dm then go to "Erotic NSFW" and ask from there**`).catch(console.error)
        }
      }
    }
  }
  
  
  

  if (messageString.charAt(0) == '?') {

    var args = messageString.substring(1).split(' ');

    var cmd = args[0];

    args = args.splice(1);

    switch(cmd) {
//Commands begin here

      case 'verify':
        if (message.channelId ==="969698388016771092"){
        var channel_name = 'verify-ticket-' + (data['verify_var'] + 1)
        data['verify_var'] = data['verify_var'] + 1

        if (!data['tickets'].find(ticket => {
          return ticket.user_id === message.member.id
        })){
          message.guild.channels.create(channel_name, {permissionOverwrites: [{id: message.author.id, allow:['VIEW_CHANNEL','SEND_MESSAGES','READ_MESSAGE_HISTORY', 'ATTACH_FILES']}, {id: "951583372285722624", allow:['VIEW_CHANNEL','SEND_MESSAGES','READ_MESSAGE_HISTORY', 'ATTACH_FILES']}, {id:'951569754718625862', deny:'VIEW_CHANNEL'}], parent: '966491997621866500'})
          .then(channel => {
            channel.send("Welcome to your `Verification ticket` <@" + message.member.id + ">. To start the Verification Process send a selfie of you holding your government issued ID / Passport / Drivers license, Make sure your Date of birth can be seen. Make sure the photo of you on your ID can also be seen. .");
            var ticket_data = {}
            ticket_data['channel_id'] = channel.id
            ticket_data['user_id'] = message.member.id
            data['tickets'].push(ticket_data)
            fs.writeFile('./data.json', JSON.stringify({"report_var" : (data['report_var']), "verify_var" : (data['verify_var'] + 1), "tickets": (data['tickets'])}), (err) => {
            if (err) console.log(err)
          })});
        }
        else{
          message.reply('you already have a ticket open').catch(console.error)
        }
      }
      message.delete().catch(console.error)
      break;

      case 'report':
        var channel_name = 'report-ticket-' + (data['report_var'] + 1)
        data['report_var'] = data['report_var'] + 1

        if (!data['tickets'].find(ticket => {
          return ticket.user_id === message.member.id
        })){
          message.guild.channels.create(channel_name, {permissionOverwrites: [{id: message.author.id, allow:['VIEW_CHANNEL','SEND_MESSAGES','READ_MESSAGE_HISTORY', 'ATTACH_FILES']}, {id: "951583372285722624", allow:['VIEW_CHANNEL','SEND_MESSAGES','READ_MESSAGE_HISTORY', 'ATTACH_FILES']}, {id:'951569754718625862', deny:'VIEW_CHANNEL'}], parent: '966491997621866500'})
          .then(channel => {
            channel.send("Welcome to your `Report ticket` <@" + message.member.id + ">. Please be patient and wait for a staff member to contact you.");
            var ticket_data = {}
            ticket_data['channel_id'] = channel.id
            ticket_data['user_id'] = message.member.id
            data['tickets'].push(ticket_data)
            fs.writeFile('./data.json', JSON.stringify({"report_var" : (data['report_var'] + 1), "verify_var" : (data['verify_var']), "tickets": (data['tickets'])}), (err) => {
            if (err) console.log(err)
          })});
        }
        else{
          message.reply('you already have a ticket open').catch(console.error)
        }
      break;
            
      case 'close':
        if (message.member.permissions.has("ADMINISTRATOR")){
          if (data['tickets'].find(ticket => {
            return ticket.channel_id === message.channel.id
          })){
            var array_location = data['tickets'].indexOf({channel_id: message.channel.id})
            data['tickets'].splice(array_location,1)
            fs.writeFile('./data.json', JSON.stringify({"report_var" : (data['report_var']), "verify_var" : (data['verify_var']), "tickets": (data['tickets'])}), (err) => {
              if (err) console.log(err)});
            message.channel.delete().catch(console.error);
            }
        }
      break;

      case 'mod':
        const mod = new MessageEmbed()
        .setColor(0xFFA500)
        .setTitle(`Moderator Application (2022)`)
        .setDescription(`**REQUIREMENTS:** \n> YOU MUST BE A LOYAL (LOYAL / LODESTAR ) \n> TRASH APPLICATIONS WILL BE DENIED \n> YOU MUST APPLY EACH CYCLE (APPS DON'T CARRY FORWARD) \n\n **Application link** \nhttps://forms.gle/EHmQjRLm3TSM7UDk7`)
        message.reply({ embeds: [mod] });
      break;

      case 'waah':
        const waah = new MessageEmbed()
        .setColor(0xFFA500)
        .setTitle(`Why cant i send gifs / links ?`)
        .setDescription(`**Q:** Why cant i send Gif's / Link's? \n\n **A:** The reason you do not have gif / link perms is because we dont want random people \n sending Discord servers on our server, Or spamming Gif's. \n\n **How do you get gif / link perms? , Well you have 3 options.** \n\n Boost the server \n **OR** \n Write a form out on why you want perms  https://forms.gle/kyGYS11sv8LDLT7HA \n **OR** \n Just deal with it and dont get perms at all.`)
        message.reply({ embeds: [waah] }).catch(console.error);
      break;

      case 'queen':
        const queen = new MessageEmbed()
        .setColor(0xFFC0CB)
        .setTitle(`**How do I become a Queen?**`)
        .setDescription(`**Step 1)** Type "?verify" in <#969698388016771092> and you will be taken to a Verification Ticket. \n\n**Step 2)** Do what the Bot tell's you to do after creating the ticket. \n\n**Step 3)** Wait for a staff member to review the photo you sent. \n\n\n *Its as simple as that, The process only takes 5 to 10 minutes.*`)
        message.reply({ embeds: [queen] }).catch(console.error);
      break;

      case 'loyal':
        const loyal = new MessageEmbed()
        .setColor(0xFFD580)
        .setTitle(`**How do I become Loyal?**`)
        .setDescription(`**Q:** Can I get the loyal role?. \n\n **A: Nah, Nope** \n\n**__Here are your two options__** \n Wait patiently for a Queen to promote you to loyal \n **or** \n Boost the server to recieve the loyal role faster.`);
        message.reply({ embeds: [loyal] }).catch(console.error);
      break;

      //Help command
      case 'cmds':
        const helpCommands = new MessageEmbed()
        .setColor(0xFFA500)
        .setTitle(`♡ **List of Commands** ♡`)
        .setDescription(`\n\n?report - *Make's a report ticket specified.* \n\n?loyal - *Tell's you how to get the loyal role sepcified.* \n\n?queen - *Tell's you how to become a queen specified* \n\n?id *<@member> - Gives user's ID specified.* \n\n?av *<@member> - Gives user's Avatar specified.* \n\n?serverinfo - *Give's servers information specified.* \n\n?afk - *Give user [AFK] prefix.* \n\n♡ **List of fun Commands** ♡ \n\n?marry *<@member> - Marries user specified.* \n\n?kiss *<@member> - Kisses member spcified.* \n\n?hug *<@member> - Hugs member specified.* \n\n?slap <@member> - *Slaps member specified.*`);
        message.reply({ embeds: [helpCommands] }).catch(console.error);
      break;

      //Admin help command
      case 'adminhelp':
        if (message.member.permissions.has("ADMINISTRATOR")){
          const adminCommands = new MessageEmbed()
          .setColor(0xFFA500)
          .setTitle(`List of Admin Commands`)
          .setDescription(`?ban <@member>- Bans user specified \n\n?kick <@member>- Kicks user specified \n\n?clear <@amount> - Clears amount of messages specified \n\n?timeout <@member @time (minutes / hours / day, max 1 day)> - Times out user for set amount of time`);
          message.reply({ embeds: [adminCommands] }).catch(console.error);
        }
      break;

      //Serverinfo command
      case 'serverinfo':
        if (message.guild != null){
          if (message.guild.members != null){
            message.guild.members.fetch('863081824401621033').then(member =>{
              becky = member.displayName;
              message.guild.roles.fetch('951583372285722624').then(role =>{
                mods = role.members.size;
                message.guild.roles.fetch('961008575721918496').then(role =>{
                  bots = role.members.size;
                  const serverInfo = new MessageEmbed()
                    .setColor(0xF058DC)
                    .setTitle(`Queen's Play info`)
                    .setDescription(`Owner: ` + becky + `\n\nTotal mods: ` + mods + `\n\nTotal bots: ` + bots + `\n\nTotal channels: ` + message.guild.channels.channelCountWithoutThreads + `\n\nTotal members: ` + message.guild.memberCount + `\n\nTotal Boosts: ` + message.guild.premiumSubscriptionCount)
                  message.reply({ embeds: [serverInfo] }).catch(console.error);
                })
              })
            })
          }
        }
      break;

      //Hug command
      case 'hug':
        mention = message.mentions.members.first();
        member = message.member
          if (mention != null){
            const hugEmbed = new MessageEmbed()
            .setColor(0xF058DC)
            .setTitle(`<3`)
            .setDescription(member.toString() + ` is hugging ` + mention.toString())
            .setImage(hugGifs[Math.floor(Math.random() * hugGifs.length)])
            message.reply({ embeds: [hugEmbed] }).catch(console.error);
          }
      break;

      //Kiss command
      case 'kiss':
        mention = message.mentions.members.first();
        member = message.member
          if (mention != null){
            const hugEmbed = new MessageEmbed()
            .setColor(0xF058DC)
            .setTitle(`<3`)
            .setDescription(member.toString() + ` is kissing ` + mention.toString())
            .setImage(kissGifs[Math.floor(Math.random() * kissGifs.length)])
            message.reply({ embeds: [hugEmbed] }).catch(console.error);
          }
      break;

      //Marry command
      case 'marry':
        mention = message.mentions.members.first();
        member = message.member
          if (mention != null){
            const hugEmbed = new MessageEmbed()
            .setColor(0xF058DC)
            .setTitle(`<3`)
            .setDescription(member.toString() + ` is marrying ` + mention.toString())
            .setImage(marryGifs[Math.floor(Math.random() * marryGifs.length)])
            message.reply({ embeds: [hugEmbed] }).catch(console.error);
          }
      break;

      //Slap command
      case 'slap':
        mention = message.mentions.members.first();
        member = message.member
          if (mention != null){
            const hugEmbed = new MessageEmbed()
            .setColor(0xF058DC)
            .setTitle(`<3`)
            .setDescription(member.toString() + ` is slapping ` + mention.toString())
            .setImage(slapGifs[Math.floor(Math.random() * slapGifs.length)])
            message.reply({ embeds: [hugEmbed] }).catch(console.error);
          }
      break;

      /**case 'servant':
        message.reply('servant sample message');
      break;

      case 'queen':
        message.reply('queen sample message');
      break;

      case 'loyal':
        message.reply('loyal sample message');
      break;

      case 'lodestar':
        message.reply('loyal sample message');
      break;

      case 'ping':
        message.reply('pong');
      break; **/

      //AFK command
      case 'afk':
        if (!message.member.permissions.has("ADMINISTRATOR")){
          if (message.member.nickname != null){
            if (message.member.nickname.startsWith("[AFK]") ){
              break;
            }
          } 
          message.reply(message.member.displayName + ' is now AFK').catch(console.error)
          message.member.setNickname("[AFK] " + message.member.displayName).catch(console.error)
        }
        
      break;

      //ID command
      case 'id':
        mention = message.mentions.members.first();
        if (mention == null){
          mention = message.member
        }
        const idMention = new MessageEmbed()
        .setColor(0x333333)
        .setTitle(mention.displayName + "'s ID")
        .setDescription(mention['id'])
        message.reply({ embeds: [idMention] }).catch(console.error);
      break;

      case 'av':
        mention = message.mentions.members.first();
        if (mention == null){
          mention = message.member
        }
        const avatarMention = new MessageEmbed()
        .setColor(0x333333)
        .setImage(mention.user.displayAvatarURL({ format: 'png' }))
        .setTitle(mention.displayName + "'s Avatar ")
        message.reply({ embeds: [avatarMention] }).catch(console.error);
      break;

      case 'kick':
        if (message.member.permissions.has("KICK_MEMBERS")) {
          mention = message.mentions.members.first();
          if (mention != null){
            if (mention.kickable){
              reason = message.content.slice(message.content.indexOf('>') + 1)
              mention.user.send("You have been kicked from Queen's Play. **Reason** " + reason).catch(err => console.log(err));
              mention.kick(reason).catch(console.error)
              message.reply(mention.displayName + ' has been kicked. **Reason:**' + reason).catch(console.error);
            }
            else {
              message.reply(mention.displayName + ' cannot be kicked').catch(console.error);
            }
          }
          else {
            message.reply('who you tryna kick?').catch(console.error);
          }
        }
        else {
          message.reply("You do not have permission to do this").catch(console.error);
        }
      break;

      case 'ban':
        if (message.member.permissions.has("BAN_MEMBERS")) {
          mention = message.mentions.members.first();
          if (mention != null){
            if (mention.bannable) {
              reason = message.content.slice(message.content.indexOf('>') + 1);
              mention.user.send("You have been banned from Queen's. **Reason:** " + reason).catch(err => console.log(err));
                mention.ban({}).catch(console.error)
              message.reply(mention.displayName + ' has been banned. **Reason:**' + reason).catch(console.error);
            }
            else {
              message.reply(mention.displayName + ' cannot be banned').catch(console.error);
            }
          }
          else{
            message.reply('who you tryna ban?').catch(console.error);
          } 
        }
        else {
          message.reply("You do not have permission to do this").catch(console.error);
        }
        
      break;

      case 'clear':
        if (message.member.permissions.has('MANAGE_MESSAGES')) {
          number = message.content.slice(message.content.indexOf(' '));
          if (!isNaN(number))
          {
            if(number > 100)
            {
              message.reply('You cannot clear more than 100 messages at once').catch(console.error)
            }
            else
            {
              message.channel.send(number + ' messages cleared')
              message.channel.messages.fetch({ limit: number })
              .then(messages => messages.each(msg => msg.delete()))
              .catch(err => console.log(err));
            }
          }
          else 
          {
            message.reply('not a number').catch(console.error)
          }
        }
        else {
          message.reply('You do not have permission to do this').catch(console.error)
        }
      break;

      case 'timeout':
        if (message.member.permissions.has('MODERATE_MEMBERS')) {
          mention = message.mentions.members.first();
          if (mention != null){
            if (mention.kickable) {
              number = message.content.slice(message.content.indexOf('>') + 1);
              number = number.trim();
              time = number.slice(number.indexOf(' ') +1)
              number = number.slice(0, number.indexOf(' '))
              if (time == 'minutes'){
                ms = 60000
              }
              else if (time == 'hours'){
                ms = 3600000
              }
              else if (time == 'day'){
                ms = 86400000
              }
              else{
                message.reply("please enter valid timeframe (minutes, hours, day)").catch(console.error)
                break;
              }
              total_time = number * ms
              if (total_time > 86400000){
                message.reply("too long, max 1 day").catch(console.error)
                break;
              }
              mention.timeout(total_time)
              message.reply(mention.displayName + " has been timed out for " + number + " " + time).catch(console.error)
            }
            else{
              message.reply("can't time out user").catch(console.error)
            }
          }
          else{
            message.reply("no user mentioned").catch(console.error)
          }
        }
        else {
          message.reply('You do not have permission to do this').catch(console.error)
        }
      break;
    }
    
  }
  if (messageString === 'agree' && message.channel.id === '951569755167408158'){
    message.member.roles.add('951570371696537601').catch(err => console.log(err))
    message.member.roles.remove('970386765787582517').catch(err => console.log(err))
  }
  if (message.channel.id === '969698388016771092' || message.channel.id === '951569755167408158'){
    message.delete().catch(err => console.log(err));
  }
});

client.on('guildMemberAdd', member => {
  if ((Date.now()- member.user.createdTimestamp) > 432000000){
    member.roles.add('970386765787582517').catch(err => console.log(err))
  }
  else {
    member.send("You have been kicked from Queen's Play as your account is too young")
          .then(message => member.kick({}))
          .catch(err => console.log(err));
  }
});

client.login("TOKEN")
